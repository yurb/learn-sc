from os import getenv

import cv2
import liblo as osc

OPENCV_PREFIX = getenv("OPENCV")

supercollider = ("127.0.0.1", 57120)
cam = cv2.VideoCapture(0)
face_detector = cv2.CascadeClassifier(
    OPENCV_PREFIX + "/share/opencv4/haarcascades/haarcascade_frontalface_alt2.xml"
)

while True:
    success, frame = cam.read()
    if success:
        #frame = cv2.flip(frame, 1)
        frame_h, frame_w, __ = frame.shape
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame = cv2.flip(frame, 1)
        frame_h, frame_w, __ = frame.shape
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_detector.detectMultiScale(gray, 1.3, 5)

        if len(faces):
            for (x, y, w, h) in faces:
                cv2.rectangle(gray, (x, y), (x + w, x + h), 255, 1)
                center_x = x + (w / 2)
                center_y = y + (h / 2)
                osc.send(
                    supercollider,
                    "/faces/face",
                    center_x / frame_w,
                    center_y / frame_h,
                    w / frame_w,
                )
        else:
            osc.send(supercollider, "/faces/none")
        cv2.imshow("camera", gray)
    if cv2.waitKey(1) == ord("q"):
        break

cam.release()
cv2.destroyAllWindows()
