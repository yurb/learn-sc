{ pkgs ? import <nixpkgs> {} }:

let
  oscpy = pkgs.python3.pkgs.buildPythonPackage rec {
    pname = "oscpy";
    version = "0.6.0";
    src = pkgs.python3.pkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-ByilpyZnMsnWRjAGPThJEdXWrkdEFrebeNSQV3P7bTM=";
    };
  };
in
pkgs.mkShell {
  buildInputs = with pkgs; [
    python3Packages.opencv4
    oscpy
  ];

  OPENCV = ''${pkgs.python3Packages.opencv4}'';
}
